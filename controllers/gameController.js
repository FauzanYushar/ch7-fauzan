var {Game} = require('../models')
var {Room} = require('../models')
var result = require('./result')


exports.start = async (req,res)=>{
    let {username} = req.params
    let {choice,no_room} = req.body
    let currentUser = req.user
    let room = await Room.findOne({where: {no_room}})
    let game = await Game.findAll({where: {no_room}})
    if(currentUser.username == username){
        if(!room) res.json({message: "room doesn't exist"})
        else{
            if (!game)
            {
                if(room.player1 == username){
                    Game.create({
                        no_room: no_room,
                        round: "1",
                        p1_choice: choice
                    })
                    .then((data)=>{
                        res.json({message: `Player 1 make a choice in Round ${data.round}`})
                    })
                }
                else if(room.player2 == username){
                    Game.create({
                        no_room: no_room,
                        round: "1",
                        p2_choice: choice
                    })
                    .then((data)=>{
                        res.json({message: `Player 2 make a choice in Round ${data.round}`})
                    })
                }
                else res.json({message: "You are not a player in this room"})
            }
            if(game){
                if(room.player1 == username){
                    game1 = await Game.findOne({where: {no_room, p1_choice: null}})
                    if (!game1){
                        if(game.length<3){
                            Game.create({
                                no_room: no_room,
                                round: (game.length+1).toString(),
                                p1_choice: choice
                            })
                            .then((data)=>{
                                res.json({message: `Player 1 make a choice in Round ${data.round}`})
                            })
                        }
                        else res.json({message: "You already play 3 round in this room"})
                    }
                    else{
                        game1.update({
                            p1_choice: choice
                        })
                        game1.update({
                            result: result.resultGame(game1.p1_choice,game1.p2_choice)
                        })
                        res.json({
                            message: `game in no room ${no_room} round ${game1.round} closed`, 
                            player_1: room.player1, 
                            player_2: room.player2,
                            result: game1.result
                         })
                    }
                }
        
                else if(room.player2 == username){
                    game2 = await Game.findOne({where: {no_room, p2_choice: null}})
                    if (!game2){
                        if(game.length<3){
                            Game.create({
                                no_room: no_room,
                                round: (game.length+1).toString(),
                                p2_choice: choice
                            })
                            .then((data)=>{
                                res.json({message: `Player 2 make a choice in Round ${data.round}`})
                            })
                        }
                        else res.json({message: "You already play 3 round in this room"})
                    }
                    else{
                        game2.update({
                            p2_choice: choice
                        })
                        game2.update({
                            result: result.resultGame(game2.p1_choice,game2.p2_choice)
                        })
                        res.json({
                            message: `game in no room ${no_room} round ${game2.round} closed`, 
                            player_1: room.player1, 
                            player_2: room.player2,
                            result: game2.result
                         })
                    }
                }
                else res.json({message: "You are not a player in this room"})
            }
        }
    }
    else res.json({message:'You are not authorized'})
}