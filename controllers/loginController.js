const { User } = require('../models' )

var format = (user) =>{
    let {id, username} = user
    return {
        id,
        username,
        accessToken: user.generateToken()
    }
}

exports.submit = (req,res)=>{
    User.authenticate(req.body)
    .then((user)=>{
        res.json(format(user))
    })
    .catch((err=>{
        res.json({message: err})
    }))
}

exports.view = (req,res) =>{
   res.render('login')
}