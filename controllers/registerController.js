var {User} = require('../models')
const passport = require('passport' )

exports.submit = async(req,res)=>{
    User.register (req.body)
    .then(() => {
        res.redirect ('/login' )
    })
    .catch((err) => next(err))
};

exports.view = (req,res)=>{
    res.render('register')
}