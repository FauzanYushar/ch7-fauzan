const {Room} = require('../models')

exports.createRoom = async (req,res)=>{
    let {username} = req.params
    let currentUser = req.user
    if(currentUser.username == username){
        let {no_room} = req.body
        let room = await Room.findOne({where: {no_room}})
        if(!room){
            Room.create({
                no_room: no_room,
                player1: username
            })
            .then((data)=>{
                res.json({message: "room created", data})
            })
        }
        else{
            if(!room.player2){
                if(room.player1 == username) res.json({message: "You are already a player in this room"})
                else{
                    room.update({
                        player2: username
                    })
                    .then((data)=>{
                        res.json({message: "You are become player 2 in this room"})
                    })
                }
            }
            else res.json({message: 'Room already full'})
        }
    }
    else res.json({message: "You are not authorized"})
}