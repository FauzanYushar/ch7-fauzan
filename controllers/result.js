exports.resultGame = (p1,p2)=>{
    if (p1==p2){
        return "draw"
    }
    if (p1=="rock"){
        if(p2=="scissor") return "player 1 win"
        else return "player 2 win"
    }
    else if (p1=="scissor"){
        if(p2=="paper") return "player 1 win"
        else return "player 2 win"
    }
    else if(p1=="paper"){
        if (p2=="rock") return "player 1 win"
        else return "player 2 win"
    }
}