var express = require('express');
var router = express.Router();
var registerController = require('../controllers/registerController.js')
var loginController = require('../controllers/loginController')
const restrict = require('../middlewares/restrict')
var crController = require('../controllers/crController')
var gameController = require('../controllers/gameController')
var getDataRouter = require('./getDataRouter')

/* GET home page. */
router.get('/register', registerController.view)
router.post('/register', registerController.submit)
router.get('/login', loginController.view)
router.post('/login', loginController.submit)
router.get('/', (req,res)=>res.redirect('login'))
router.post('/:username/create-room', restrict, crController.createRoom)
router.post('/:username/fight',restrict, gameController.start)
router.use('/getData',getDataRouter)

module.exports = router;
