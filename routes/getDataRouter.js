var router = require("express").Router()
var getDataController = require("../controllers/getDataController")
var restrict = require('../middlewares/restrict')

router.get('/:username/user',restrict,getDataController.user)
router.get('/:username/room',restrict,getDataController.room)
router.get('/:username/game',restrict,getDataController.game)

module.exports = router
